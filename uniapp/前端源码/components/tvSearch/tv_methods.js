const MyApp = getApp();
const globalData = MyApp.globalData;
// const lyzmlDLNA = uni.requireNativePlugin("lyzml-DLNA");
export default {
    computed: {
        i18n() {
            return globalData.$t('detail');
        }
    },
    data() {
        return {
            windowInfo: {
                statusBarHeight: 0,
                windowHeight: 0,
                windowWidth: 0,
                trueHeight: 0
            },
            modalStyle: '',
            myEventManager: null,
            eventManager: null,
            tvLists: [],
            isshow: true,
            searchText: globalData.$t('detail').searchText,
            toastTitle: globalData.$t('detail').toastTitle,
            showLoading: true,
            isStop: true,
            btnText: globalData.$t('detail').btnText,
            isClose: false,
            hasHistory: false,
            historyList: null
        };
    },
    mounted() {
        var _this = this;
        _this.$nextTick(function() {
            _this.init();
        })
    },
    methods: {
        tvInit() {
            let _this = this;
            _this.myEventManager = null;
            _this.eventManager = null;
            _this.tvLists = [];
            _this.isshow = true;
            _this.toastTitle = globalData.$t('detail')
                .toastTitle;
            _this.showLoading = true;
            _this.isStop = true;
            _this.btnText = globalData.$t('detail').btnText;
            _this.searchText = globalData.$t('detail')
                .searchText;
            _this.isClose = false;
            _this.startSearch();
        },
        init() {
            var _this = this;
            const res = uni.getSystemInfoSync();
            _this.windowInfo.statusBarHeight = res.statusBarHeight;
            _this.windowInfo.windowHeight = res.windowHeight;
            _this.windowInfo.trueHeight = _this.windowInfo.windowHeight - _this
                .windowInfo.statusBarHeight;
            // modal样式
            _this.modalStyle = 'height:' + (_this.windowInfo.trueHeight - (_this
                    .windowInfo.trueHeight / 2.2)) +
                'px; overflow-y: auto;';
        },
        startSearch() {
            this.$refs.popup && this.$refs.popup.open();
            MyApp.$sendTv.startSearch((devList) => {
                let tmp = [],
                    _tmp = [];
                devList.map((v, k) => {
                    tmp.push(v.name);
                });
                tmp = this.unique(tmp);
                tmp.map((v, k) => {
                    devList.map((val) => {
                        if (v == val.name) {
                            _tmp[k] = val;
                        }
                    });
                });
                this.tvLists = _tmp;
                let historyInfo = uni.getStorageSync('tvHistory');
                if (historyInfo) {
                    this.historyInfo = historyInfo;
                    this.hasHistory = true;
                }
                console.log("====startSearch====", JSON.stringify(
                    devList));
            });
        },
        unique(array) {
            var n = {},
                r = [],
                len = array.length,
                val, type;
            for (var i = 0; i < array.length; i++) {
                val = array[i];
                type = typeof val;
                if (!n[val]) {
                    n[val] = [type];
                    r.push(val);
                } else if (n[val].indexOf(type) < 0) {
                    n[val].push(type);
                    r.push(val);
                }
            }
            return r;
        },
        hideModal() {
            this.$refs.popup.close();
            MyApp.$sendTv.stopSearch();
            this.showLoading = false;
            this.isClose = true;
        },
        sendTotv(index) {
            let _this = this,
                ip = typeof(index) == 'object' ? index.ip : _this.tvLists[index]
                .ip;
            MyApp.$sendTv.playVideo({
                ip: ip,
                mediaURL: _this.videoUrls
            }, (resp) => {
                console.log("====playVideo=====", _this.videoUrls);
                if (resp.code == 0) {
                    _this.stopSearch();
                    _this.hideModal();
                    uni.$emit('showTvMask');
                    uni.setStorageSync('tvHistory', typeof(index) ==
                        'object' ? index : _this.tvLists[index]);
                }
            });
        },
        stopVideos() {
            console.log("====stopVideo====");
            MyApp.$sendTv.stopVideo();
        },
        stopSearch() {
            if (this.isStop) {
                MyApp.$sendTv.stopSearch();
                this.toastTitle = globalData.$t('detail').toastTitle;
                this.searchText = globalData.$t('detail').endsearchText;
                this.showLoading = false;
                this.btnText = globalData.$t('detail').rebtnText;
                this.isStop = false;
            } else {
                this.startSearch();
                this.btnText = globalData.$t('detail').btnText;
                this.searchText = globalData.$t('detail').searchText;
                this.isStop = true;
                this.showLoading = true;
            }
        }
    }
}
