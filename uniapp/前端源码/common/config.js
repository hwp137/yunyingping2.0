let websiteSecret = '',
    websiteUrl = '',
    apiImageUrl = '',
    imageUrl = '',
    shareUrl = '',
    appName = '',
    tipImg = {};


// ------------- 修改区域开始 -------------//

// // 通讯密匙
websiteSecret = '';
// api系统接口地址 xxxx 更换为接口系统域名
websiteUrl = 'https://xxxx/api.php';
// api系统域名  xxxx 更换为接口系统域名
apiImageUrl = 'https://xxxx';
// cms系统域名  xxxx 更换为cms系统域名
imageUrl = 'http://xxxx/';
// 小程序名称
appName = '云影评2.0';


// 提示收藏图片，可以远程图片，也可以本地
tipImg = {
    tip: '点击「·•·」，下次访问更便捷',
    img1: '',
    img2: '',
    img3: '',
};

// ------------- 修改区域结束 -------------//

// 测试功能，APP分享到微信的H5页面地址，需配置微信开放平台APPID
shareUrl = '';

const cachePath = '/cache/';
var getUrlName = function getUrlName(url) {
    let tmp = new Array(); //临时变量，保存分割字符串
    tmp = url.split("/"); //按照"/"分割
    let pp = tmp[tmp.length - 1]; //获取最后一部分，即文件名和参数
    tmp = pp.split("?"); //把参数和文件名分割开
    return tmp[0];
}

export default {
    websiteSecret,
    websiteUrl,
    imageUrl,
    apiImageUrl,
    cachePath,
    getUrlName,
    appName,
    shareUrl,
    tipImg,
}
