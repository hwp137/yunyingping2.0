const MyApp = getApp();
const globalData = MyApp.globalData;
import requestApi from '@/common/vmeitime-http/http.js';
import configs from '@/common/config.js';
import md5 from '@/common/md5.js';
import util from '@/utils/utils.js';
export default {
    data() {
        return {
            statusMaskTop: '',
            commentInput: {
                isInput: false,
                keyHeight: 0,
                isFocus: false,
                content: '',
                sendCommentTextColor: '#F0F0F0',
                tips: ''
            },
            commentLists: [],
            commentForm: {
                oldPage: 0,
                page: 1,
                limit: 10,
                isEnd: false,
                loadStatus: 'loadmore',
                scrollLock: false
            },
            light: false,
            screenWidth: '',
            pages: '',
            playerAd: {
                img: '',
                url: ''
            },
            playerBottomAD: {
                img: '',
                url: '',
                isshow: 0
            },
            videoUrl: '',
            imageUrl: configs.imageUrl,
            apiImageUrl: configs.apiImageUrl,
            poster: '',
            currentTime: 0,
            danmuList: [{
                text: globalData.$t('detail').videoWaringText,
                color: '#ff0000',
                time: 1
            }],
            videoStyle: '',
            // 状态栏高度，H5中，此值为0，因为H5不可操作状态栏
            statusBarHeight: uni.getSystemInfoSync().statusBarHeight,
            // 导航栏内容区域高度，不包括状态栏高度在内
            navbarHeight: 44,
            windowInfo: {
                statusBarHeight: 0,
                windowHeight: 0,
                windowWidth: 0,
                trueHeight: 0
            },
            contentStyle: '',
            loadingMaskStyle: '',
            vid: 0,
            videoHeight: 0,
            info: '',
            downloadList: [],
            videoList: [],
            index: 0,
            videoInfo: {
                srcList: [{
                    name: '',
                    url: ''
                }],
                recommend: [],
                title: '',
                playIndex: 0,
                liveMode: false,
                remark: '',
                type: 0,
                currentTime: 0,
                danmuList: [],
                downLoadList: [],
                vod_pic: ''
            },
            TabCur: 0, // 当前集数
            vodPlayFromTab: 0, // 当前播放器索引
            isShowxx: false,
            scrollLeft: 0,
            lang: null,
            changeYuan: false,
            isFullscreen: false,
            trueUrl: '',
            isFirst: true
        };
    },
    computed: {
        i18n() {
            return globalData.$t('detail');
        },
        i18n_common() {
            return globalData.$t('common');
        }
    },
    onLoad(option) {
        this.adInfo = uni.getStorageSync('adInfo') || 0;
        this.vid = option.id;
        if (!this.vid) {
            uni.showModal({
                title: globalData.$t('common').modelTitle,
                content: globalData.$t('common').noSearchInfo,
                showCancel: false,
                success() {
                    uni.navigateBack({});
                }
            });
            return false;
        }
        if (option && option.tab) {
            this.TabCur = option.tab;
        }
        if (option && option.froms) {
            this.froms = 1;
        }
        if (option && option.index) {
            this.TabCur = option.index;
        }
        let sys = uni.getStorageSync('sys_config');
        this.poster = this.apiImageUrl + sys.video_poster;

    },
    onShow() {
        this.tabIndex = uni.getStorageSync('tabIndex') || 0;
    },
    onHide() {

    },
    onReady() {
        this.initPage();
    },
    onUnload() {
        this.$refs.adAlertImgPopup.close();
        uni.$off('tvModelInit');
    },
    mounted() {
        let _this = this;
        _this.$nextTick(function() {
            // 注册自定义事件
            _this.eventFunc();
            // 获取广告信息
            _this.getAdinfo();
            uni.onKeyboardHeightChange(res => {
                if (res.height > 0) {
                    _this.commentInput.keyHeight = res.height;
                } else {
                    this.commentInput.isFocus = false;
                    this.commentInput.isInput = false;
                    this.commentInput.keyHeight = 0;
                }
            });
        });
    },
    watch: {
        videoUrl(newVal, oldVal) {
            let _this = this;
            if ((newVal != '') || _this.changeYuan) {
                _this.getAnalysis(newVal);
            }
        }
    },

    methods: {
        initComment() {
            this.commentForm = {
                oldPage: 0,
                page: 1,
                limit: 10,
                isEnd: false,
                loadStatus: 'loadmore',
                scrollLock: false
            }
            this.commentInput = {
                isInput: false,
                keyHeight: 0,
                isFocus: false,
                content: '',
                sendCommentTextColor: '#F0F0F0',
                tips: ''
            };
            uni.hideKeyboard();
            this.commentLists = [];
        },
        getAnalysis: function(newVal) {
            let _this = this;
            requestApi.post('/videos/analysis/getAnalysis', {
                    url: MyApp.tools.encode(encodeURIComponent(
                        newVal)),
                    vid: _this.vod_id,
                    vtitle: _this.videoInfo.title
                })
                .then(res => {
                    if (res.code != 200) {
                        return false;
                    }
                    _this.isFirst = false;
                    _this.changeYuan = false;
                    _this.trueUrl = res.data;
                    // _this.$refs.videoPlayer.changSrc(res.data);
                })
                .catch(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                    _this.loading = false;
                });

        },
        sendComment() {
            let _this = this;
            MyApp.checkLogin(true, function(userinfo) {
                if (_this.commentInput.content == '') {
                    return false;
                }
                if (_this.commentInput.tips != '') {
                    return false;
                }
                requestApi.post('/videos/replay/sendReplay', {
                        avatar: MyApp.tools.encode(encodeURIComponent(userinfo.avatar)),
                        vod_id: _this.vod_id,
                        vod_name: _this.navTitle,
                        content: _this.commentInput.content,
                        nickname: userinfo.nick,
                    })
                    .then(res => {
                        uni.showToast({
                            title: res.msg || (res.code != 200 ? '发布失败' : '发布成功'),
                            icon: 'none'
                        })
                        if (res.code != 200) {
                            return false;
                        }
                    }).then(v => {
                        _this.initComment();
                        _this.getReplayList();
                    })
                    .catch(res => {
                        console.log(res);
                    });
            });
        },
        commentInputKeyblur() {
            this.commentInput.isFocus = false;
            this.commentInput.isInput = false;
            this.commentInput.keyHeight = 0;
            uni.hideKeyboard();
        },
        onCommentInput() {
            let _this = this;
            setTimeout(v => {
                _this.commentInput.isFocus = true;
                _this.commentInput.isInput = true;
            }, 400)
        },
        commentInputSth(e) {
            if (e.detail.value != "") {
                this.commentInput.content = e.detail.value;
                if (this.commentInput.content.length > 500) {
                    this.commentInput.tips = '已超出 ' + Math.abs(500 - this.commentInput.content.length) + ' 个字';
                    this.commentInput.sendCommentTextColor = "#F0F0F0";
                } else {
                    this.commentInput.tips = '';
                    this.commentInput.sendCommentTextColor = "#E0584B";
                }
            } else {
                this.commentInput.sendCommentTextColor = "#F0F0F0";
            }
        },
        closeIntroPopup() {
            this.$refs.introPopup.close();
        },
        introPopup() {
            this.$refs.introPopup.open();
        },
        bulbSwitch() {
            if (this.light) {
                this.pages = 'background-color: #F5F6F7; min-height: ' + this
                    .windowInfo.windowHeight + 'px';
            } else {
                this.pages = 'background-color: #282C35; min-height: ' + this
                    .windowInfo.windowHeight + 'px';
            }
            this.light = !this.light;
        },
        tabSelect(e) {
            if (e.currentTarget.dataset.id != this.TabCur) {
                this.TabCur = e.currentTarget.dataset.id;
                // this.scrollLeft = (e.currentTarget.dataset.id - 1) * 60;
                this.currentTime = 0;
                this.isShowModel = false;
                uni.setStorageSync(md5(this.videoInfo.title + this.TabCur),
                    0);
                this.changeVideoInfo(e.currentTarget.dataset.id);
            }
        },
        tabPlayFromSelect(e) {
            let _this = this;
            if (e != _this.vodPlayFromTab) {
                _this.vodPlayFromTab = e;
                // _this.TabCur = -1;
                _this.changeVideoInfo(_this.TabCur);
            }
        },
        getAdinfo() {
            let _this = this;
            _this.adInfo = uni.getStorageSync('adInfo') || 0;
            // 获取配置信息
            _this.getSysConfig();
        },
        showAd() {
            let _this = this;
            let sysconf = _this.sysconf;
            // 是否显示弹窗广告
            if (sysconf.player_show_ad == 1) {
                if (_this.adInfo && _this.adInfo.alert_ad.img) {
                    if (_this.adInfo.alert_ad.ad_type != 5) {
                        _this.playerAd = _this.adInfo.alert_ad;
                        _this.$refs.adAlertImgPopup.open();
                    }
                }
                if (sysconf.player_bottom_ad == 1 && _this.adInfo && _this
                    .adInfo.alert_public.img) {
                    _this.playerBottomAD = _this.adInfo.alert_public;
                    _this.playerBottomAD.img = _this.apiImageUrl + _this.adInfo
                        .alert_public.img;
                    _this.playerBottomAD.url = _this.adInfo.alert_public.url;
                    _this.playerBottomAD.isshow = 1;
                }
            }

            // 底部广告
            if (sysconf.player_bottom_ad == 1) {
                if (_this.adInfo && _this.adInfo.alert_public.length > 0) {
                    _this.playerBottomAD = _this.adInfo.alert_public;
                    _this.playerBottomAD.img = _this.apiImageUrl + _this.adInfo
                        .alert_public.img;
                    _this.playerBottomAD.url = _this.adInfo.alert_public.url;
                    _this.playerBottomAD.isshow = 1;
                } else {
                    _this.playerBottomAD.isshow = 0;
                }
                if (sysconf.show_video_ad == 1 && _this.adInfo && _this.adInfo
                    .video_ad.hasOwnProperty('img')) {
                    if (_this.$isMpWeixin) {
                        _this.videoAd = _this.adInfo.video_ad.url;
                    }
                }
            }
            // 视频容器
            _this.playVideo();
        },
        getSysConfig() {
            var _this = this;
            requestApi.post('/videos/config/getConfig', {}).then(res => {
                let sys;
                if (res.code != 200) {
                    sys = uni.getStorageSync('sys_config');
                } else {
                    sys = res.data;
                    uni.setStorageSync('sys_config', res.data);
                }
                _this.sysconf = sys;
                _this.showCopy = _this.sysconf.copy_url;
                _this.isShowxx = _this.sysconf.show_xx ? true : false;
                // 会员等级
                _this.memberGroupLimit(res.data.userinfo);
                // 广告
                _this.showAd();
            });
        },
        memberGroupLimit(userinfo) {
            let _this = this;
            // 是否开启了会员等级控制
            if (_this.sysconf.member_group_switch) {
                // 如果当前不是审核状态
                if (!_this.isShowxx) {
                    // 如果当前登录会员等级低于后台设定等级，不予播放;如果未登录，不予播放；如果用户被锁定，不予播放
                    if (!userinfo.hasOwnProperty('group_id') || userinfo
                        .group_id < _this.sysconf
                        .member_group_limit || userinfo.islock) {
                        _this.isShowxx = true;
                    } else {
                        _this.member_group = userinfo.group_id;
                        _this.froms = 0;
                    }
                } else {
                    // 如果当前已登录，是审核状态，但是会员等级高于设定等级并且未被锁定
                    if (userinfo.hasOwnProperty('group_id') && userinfo
                        .group_id >= _this.sysconf
                        .member_group_limit && !userinfo.islock) {
                        _this.member_group = userinfo.group_id;
                        _this.isShowxx = false;
                        _this.froms = 0;
                    }
                }
            } else {
                if (userinfo.hasOwnProperty('group_id') && userinfo.islock) {
                    _this.member_group = userinfo.group_id;
                    _this.isShowxx = true;
                }
                _this.froms = 0;
            }
        },
        eventFunc() {
            let _this = this;
            // 下一集
            uni.$on('playNext', function(val) {
                _this.isFullScreen = val.isFullScreen;
                // 如果是剧集，播放完成自动播放下一集
                if (_this.videoInfo.type != 1) {
                    let index = _this.TabCur + 1;
                    if (index < _this.videoInfo.srcList[_this
                            .vodPlayFromTab].length) {
                        _this.currentTime = 0;
                        _this.changeVideoInfo(index);
                    }
                }
            });
            uni.$on('timeUpdate', function(e) {
                _this.playing(e);
            });
            uni.$on('getMore', function(e) {
                _this.getMores(e);
            });

        },
        changeVideoInfo(index) {
            let _this = this;
            _this.changeYuan = true;
            _this.TabCur = index;
            if (!_this.isFullScreen) {
                _this.trueUrl = '';
            }

            if (!((!_this.isShowxx || (_this.isShowxx && !_this.$isMp)) && !
                    _this.showAdModel && !_this
                    .froms) || _this
                .videoInfo.title) {
                _this.loading = false;
            }

            // 转发分享
            MyApp.share.hard = 1;
            if (MyApp.shareTimeLineParams.indexOf('&tab=') == -1) {
                MyApp.shareTimeLineParams = MyApp.shareTimeLineParams +
                    '&tab=' + _this.TabCur;
            } else {
                MyApp.shareTimeLineParams = MyApp.shareTimeLineParams.replace(
                        /&tab=[0-9]/g, '&tab=') + _this
                    .TabCur;
            }
            if (MyApp.share.path.indexOf('&tab=') == -1) {
                MyApp.share.path = MyApp.share.path + '&tab=' + _this.TabCur;
            } else {
                MyApp.share.path = MyApp.share.path.replace(/&tab=[0-9]/g,
                    '&tab=') + _this.TabCur;
            }

            _this.videoUrl = '';
            _this.videoUrl = _this.videoInfo.srcList[_this.vodPlayFromTab][
                index
            ].url;

            let title =
                _this.videoInfo.title + (_this.isShowxx ? '' : ' - ' + _this
                    .videoInfo.srcList[_this
                        .vodPlayFromTab][
                        index
                    ].name);

            _this.navTitle = title;
            MyApp.share.title = title;

            uni.setNavigationBarTitle({
                title: title
            });
        },
        goAd() {
            plus.runtime.openWeb(this.playerAd.url);
        },
        goBottomAd() {
            let _this = this;
            plus.runtime.openWeb(this.playerBottomAD.url);
        },
        playerADCancel() {
            let _this = this;
            // 是否显示弹窗广告
            if (_this.sysconf.ad_close == 1) {
                _this.goAd();
            } else {
                _this.$refs['image'].close();
                _this.showAdModel = false;
                _this.drowVideoBox();
            }
        },
        showLoading(title) {
            uni.showLoading({
                title: title || _this.i18n.loadingText,
                mask: true
            });
        },
        initPage() {
            let _this = this;
            const res = uni.getSystemInfoSync();
            _this.windowInfo.statusBarHeight = res.statusBarHeight;
            _this.windowInfo.windowWidth = res.windowWidth;
            _this.windowInfo.windowHeight = res.windowHeight;
            _this.windowInfo.trueHeight = _this.windowInfo.windowHeight - _this
                .windowInfo.statusBarHeight - 50;
            _this.videoHeight = _this.windowInfo.trueHeight / 3;
            
            _this.screenWidth = 'width:' + _this.windowInfo.windowWidth * 2 +
                'upx';
            _this.contentStyle = 'height:' + (_this.windowInfo.windowHeight -
                _this.videoHeight) * 2 + 'upx';
            
            _this.loadingMaskStyle = 'height:' + (_this.windowInfo.windowHeight - _this.videoHeight) + 'px';
            
            _this.videoStyle = 'width: 750rpx;height:' + _this.videoHeight + 'px;';
            
            _this.pages = 'background-color: #F5F6F7; min-height: ' + _this
                .windowInfo.windowHeight + 'px';

            _this.initComment();
        },
        playVideo() {
            let _this = this;
            requestApi
                .post('/videos/videos/getVideoDetail', {
                    vid: _this.vid,
                    index: _this.TabCur,
                    app: 1
                })
                .then(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                    if (res.code != 200 || !res.data) {
                        uni.showModal({
                            content: res.data.msg || globalData.$t(
                                'detail').notFindVideoInfo,
                            showCancel: false,
                            success() {
                                uni.navigateBack();
                            }
                        });
                        return false;
                    }
                    _this.vod_id = res.data.vid;
                    _this.videoInfo.srcList = res.data.srcList;
                    _this.videoInfo.title = res.data.title;
                    _this.videoInfo.vod_pic = res.data.vod_pic;
                    _this.videoInfo.vod_time = res.data.vod_time;
                    _this.videoInfo.vod_year = res.data.vod_year;
                    _this.videoInfo.vod_actor = res.data.vod_actor;
                    _this.videoInfo.remark = res.data.remark;
                    _this.videoInfo.type = res.data.type;
                    _this.videoInfo.count = res.data.count;
                    _this.danmuList.push(res.data.danmuList);
                    _this.vodPlayFrom = res.data.vod_play_from;
                    _this.vodPlayFromTab = _this.vodPlayFrom[0];
                    // _this.videoInfo.downLoadList = res.data.downLoadList;
                    _this.videoInfo.vod_score = res.data.vod_score;
                    _this.isLike = res.data.isLike;
                    _this.currentTime = uni.getStorageSync(md5(_this
                        .videoInfo.title + _this.TabCur));

                    // 转发分享
                    MyApp.share.title = _this.videoInfo.title;
                    MyApp.share.imageUrl = _this.videoInfo.vod_pic.search(
                            'http') >= 0 ?
                        _this.videoInfo.vod_pic : _this.imageUrl + _this
                        .videoInfo.vod_pic;
                    MyApp.share.desc = _this.videoInfo.title;
                    MyApp.share.content = _this.videoInfo.title;

                    _this.getReplayList();

                    _this.titleNview(res.data.title);

                    _this.changeVideoInfo(_this.TabCur)
                })
                .catch(err => {
                    uni.hideLoading();
                    _this.loading = false;
                });
        },
        titleNview(title) {
            let pages = getCurrentPages();
            let page = pages[pages.length - 1];
            let currentWebview = page.$getAppWebview();
            let tn = currentWebview.getStyle().titleNView;
            tn.titleText = title;
            currentWebview.setStyle({
                titleNView: tn
            });
        },
        playing(event) {
            uni.setStorageSync(md5(this.videoInfo.title + this.TabCur),
                event.detail.currentTime);
            this.VodLoadText = '';
            MyApp.logHistory({
                vod_pic: this.videoInfo.vod_pic,
                title: this.videoInfo.title,
                vod_id: this.vod_id,
                index: this.TabCur,
                type: this.videoInfo.type
            });
        },
        getReplayList() {
            let _this = this;
            if (_this.commentForm.oldPage == _this.commentForm.page) {
                return false;
            }
            if (!_this.commentForm.isEnd && !_this.commentForm.scrollLock) {
                let form = {
                    page: _this.commentForm.page,
                    limit: _this.commentForm.limit,
                    vid: _this.vid,
                }
                _this.commentForm.scrollLock = true;
                requestApi
                    .post('/videos/replay/getVodReplay', form)
                    .then(async res => {
                        _this.commentForm.oldPage = _this.commentForm.page;
                        _this.commentForm.scrollLock = false;
                        let tmp;
                        if (res.data.list && res.data.list.length > 0) {
                            res.data.list.forEach((v, k) => {
                                tmp = {
                                    avatar: v.avatar ? v.avatar : '../../static/icons/avatar-none.png',
                                    nickname: v.nickname,
                                    content: v.content,
                                    create_time: v.create_time,
                                    lines: 3,
                                    openMoreText: '◢ 展开'
                                }
                                _this.commentLists.push(tmp);
                            });
                            _this.commentForm.loadStatus = 'loadmore';
                        } else {
                            _this.commentForm.isEnd = true;
                            _this.commentForm.page = 1;
                            _this.commentForm.loadStatus = 'nomore';
                        }
                    }).catch(res => {
                        console.log(res);
                    })
            }
        },
        getMores() {
            this.commentForm.page = this.commentForm.page + 1;
            this.getReplayList();
        },
        onShare() {
            MyApp.$u.toast('开发中');
        },
        onReport() {
            let _this = this,
                params = {
                    vod_play_from: _this.vodPlayFromTab,
                    vod_id: _this.vod_id,
                    vod_name: _this.videoInfo.title,
                    vod_index: _this.videoInfo.srcList[_this.vodPlayFromTab][
                        _this.TabCur
                    ].name,
                    remark: ''
                };
            uni.showModal({
                title: '提示',
                content: '如果当前视频无法播放，点击“一键反馈”给我们，我们将处理该问题',
                confirmText: '一键反馈',
                success: function(res) {
                    if (res.confirm) {
                        requestApi
                            .post('/videos/report/report', params)
                            .then(res => {
                                if (200 == res.code) {
                                    uni.showToast({
                                        title: '反馈成功，感谢支持',
                                        mask: true,
                                        icon: 'none'
                                    })
                                }
                            })
                    }
                }
            })
        },
        onKefu() {
            let _this = this,
                tmp = [];
            tmp.push(_this.apiImageUrl + _this.sysconf.kefu_qrcode);
            uni.previewImage({
                urls: tmp,
                longPressActions: {
                    itemList: ['保存图片'],
                    success: function(data) {
                        uni.saveImageToPhotosAlbum({
                            filePath: tmp[0],
                            success: function () {
                                MyApp.$u.toast('保存成功');
                            }
                        });
                    },
                    fail: function(err) {

                    }
                },
                complete(res) {
                    console.log(res);
                }
            });
        },


        // ---------------- 下面是试验中功能 ---------
        download() {
            this.$refs.videoPlayer.download(
                'http://baobab.kaiyanapp.com/api/v1/playUrl?vid=164016&resourceType=video&editionType=low&source=aliyun&playUrlType=url_oss'
            );
        },
        clickDownload(e) {
            if (!this.videoList[e.idx - 1].download) {
                let frist = true;
                //继续下载
                for (let item of this.downloadList) {
                    if (item.epi == e.idx && item.text != '完成') {
                        item.text = '暂停';
                        item.task.start();
                        frist = false;
                    }
                }
                this.videoList[e.idx - 1].download = true;
                if (!frist) return;
                //创建下载
                const downloader = {
                    epi: e.idx, //集数
                    task: null,
                    progress: 0,
                    text: '暂停',
                    filename: '',
                    index: 0
                };
                this.downloadList.push(downloader);
                let idx = this.downloadList.length - 1;
                let localName = {};
                //m3u8不处理 标记格式
                localName = this.getSaveName(e.src, this.videoList[e.idx - 1]
                    .title);
                this.downloadList[idx].task = plus.downloader.createDownload(e
                    .src, localName, (d, status) => {
                        // 下载完成
                        if (status == 200) {
                            this.downloadList[idx].text = '打开';
                            //保存本地视频至list
                            let filename = plus.io
                                .convertLocalFileSystemURL(d.filename);
                            const video = {
                                ...this.videoList[e.idx - 1]
                            };
                            video.src = filename;
                            video.title = `本地视频第${e.idx}集`;
                            video.currentTime = 0;
                            this.videoList.push(video);
                            this.downloadList[idx].filename = filename;
                            this.videoList[e.idx - 1].download = false;
                            //保存索引用于跳转
                            this.downloadList[idx].index = this.videoList
                                .length;
                            this.setGlobalData();
                            uni.showToast({
                                title: `下载成功,文件保存至${filename}`,
                                icon: 'none'
                            });
                        } else {
                            console.log('下载失败');
                        }
                    });
                //监听下载
                this.downloadList[idx].task.addEventListener(
                    'statechanged',
                    (download, status) => {
                        let progress = isNaN(download.downloadedSize /
                                download.totalSize) ?
                            0 :
                            (download.downloadedSize / download.totalSize) *
                            100;
                        this.downloadList[idx].progress = Math.floor(
                            progress);
                    },
                    false
                );
                this.downloadList[idx].task.start();
            } else {
                //暂停
                for (let item of this.downloadList) {
                    if (item.epi == e.idx && item.text != '完成') {
                        this.videoList[e.idx - 1].download = false;
                        item.text = '继续';
                        item.task.pause();
                    }
                }
            }
        },
        clickDownloadBtn(idx, itm) {
            if (itm.text == '继续') {
                itm.text = '暂停';
                this.videoList[itm.epi - 1].download = true;
                itm.task.start();
            } else if (itm.text == '暂停') {
                itm.text = '继续';
                this.videoList[itm.epi - 1].download = false;
                itm.task.pause();
            } else {
                this.playEpi(itm.index);
            }
        },
        // 暂停下载任务
        pauseDownload(idx) {
            this.downloadList[idx].task.pause();
        },
        // 取消下载任务
        abortDownload(idx) {
            this.downloadList[idx].task.abort();
        },
        setGlobalData() {
            let length = 0;
            for (let item of this.downloadList) {
                if (item.text == '打开') {
                    length++;
                }
            }
            getApp().globalData.downloadLength = length;
        },
        playEpi(val) {
            this.index = val;
        },
        getSaveName(src, title) {
            if (/\.m3u8$/.test(src)) {
                return '';
            } else {
                return {
                    filename: `_downloads/${title}.mp4`
                };
            }
        },
        isEmpty(obj) {
            if (typeof obj == 'undefined' || obj == null || obj == '') {
                return true;
            } else {
                return false;
            }
        }
    }
};
