const MyApp = getApp();
const globalData = MyApp.globalData;
import autoUpdater from '@/common/autoUpdater.min.js';
import util from '@/utils/utils.js';
export default {
    computed: {
        i18n() {
            return globalData.$t('index');
        },
        _i18n: {
            set() {
                return globalData.$t;
            },
            get() {
                return globalData.$t;
            }
        }
    },
    data() {
        return {
            dragHide: false,
            typelist: globalData.typelist,
            tarbarObj: globalData.tarbarObj,
            backGroundColor: '',
            backGroundClass: '',
            recommendVideos: [],
            hotVideos: [],
            ihotVideos: [],
            acgViedeos: [],
            varietyViedeos: [],
            msg: [],
            msgList: [],
            popup: {
                popupImg: '',
                popupTitle: '',
                popupStyle: '',
                popupType: 0
            },
            imageUrl: this.$config.imageUrl,
            apiImageUrl: this.$config.apiImageUrl,
            appName: this.$config.appName,
            swiperheight: 0,
            tjvod: [],
            typevod: [],
            cateList: [],
            bannerList: [],
            isShowxx: 0,
            recommend: {
                page: 1,
                limit: 9,
                isEnd: false
            },
            loading: false,
            sysconfig: [],
            dragInfo: [],
            dragStyle: ''
        };
    },
    mounted () {
        this.$nextTick(() => {
            this.getPopupInfo();
        })
    },
    onReady() {
        this.initPage();
    },
    onShow() {
        this.sysconfig = uni.getStorageSync('sys_config');
        this.isShowxx = this.sysconfig.show_xx;
        this.dragInfo = this.sysconfig.drag;
        if (this.dragInfo && this.dragInfo.drag_btn_img) {
            this.dragStyle = 'background-image: url("' + (this.apiImageUrl + this.dragInfo.drag_btn_img) + '");background-size: contain';
        }
    },
    methods: {
        checkUpdate() {
            var _this = this;
            _this.$http.post('/videos/config/updateInfo', {}).then(res => {
                if (res.code == 200) {
                    let versionInfo = res.data;
                    let a_ver = null;
                    switch (_this.$os) {
                        case 'android':
                            a_ver = versionInfo.android;
                            break;
                        case 'ios':
                            a_ver = versionInfo.ios;
                            break;
                    }
                    let ver = plus.runtime.version.split('.').join('');
                    if (a_ver.version.split('.').join('') > ver && a_ver.atOnce == true) {
                        autoUpdater.init({
                            packageUrl: a_ver.download_url,
                            content: a_ver.log,
                            contentAlign: 'left',
                            cancel: '取消该版本',
                            cancelColor: '#007fff'
                        });
                        autoUpdater.show();
                    }
                }
            });
        },
        goad(index) {
            if (this.$isH5) {
                window.open();
            } else {
                plus.runtime.openWeb();
            }
        },
        showGonggao(e) {
            let msgInfo = this.msgList[e];
            uni.showModal({
                title: msgInfo.title,
                content: msgInfo.content,
                showCancel: false
            });
        },
        goCateDetail(type) {
            switch (type) {
                case 1:
                    uni.navigateTo({
                        url: '/packageA/cate/lists?type=1'
                    });
                    break;
                case 2:
                    uni.navigateTo({
                        url: '/packageA/cate/lists?type=2'
                    });
                    break;
                case 3:
                    uni.navigateTo({
                        url: '/packageA/cate/lists?type=3'
                    });
                    break;
                case 4:
                    uni.navigateTo({
                        url: '/packageA/cate/lists?type=4'
                    });
                    break;
                default:
                    return false;
            }
        },
        goDetail(item = 0, banner = false) {
            uni.setStorageSync('goback', '/pages/index/index');
            if (banner) {
                if (item.isad == 1) {
                    if (this.$isAppPlus) {
                        plus.runtime.openWeb(item.ad_url);
                    }
                } else {
                    if (parseInt(item.vid) > 0) {
                        MyApp.godetail(item.vid);
                    }
                }
            } else {
                if (parseInt(item) > 0) {
                    MyApp.godetail(item);
                }
            }
        },
        search() {
            uni.switchTab({
                url: '/pages/search/index'
            });
        },
        getbannerList() {
            var _this = this;
            _this.$http.post('/banner/banner/getBannerLists', {}).then(res => {
                let data = res.data.list;
                if (!_this.$isMpWeixin) {
                    let tmp = [];
                    data.forEach((v, k) => {
                        data[k].img = v.img.search('http') >= 0 ? v.img : _this.apiImageUrl + v.img;
                        if (v.isad < 2) {
                            tmp.push(data[k]);
                        }
                    });
                    _this.bannerList = tmp;
                } else {
                    data.forEach((v, k) => {
                        data[k].img = v.img.search('http') >= 0 ? v.img : _this.apiImageUrl + v.img;
                    });
                    _this.bannerList = data;
                }
            }).then(v => {
                _this.getVideoList();
            }).catch(res => {
                _this.getVideoList();
            });
        },
        getMsgList() {
            var _this = this;
            _this.$http.post('/videos/home/getMsgLists', {}).then(res => {
                _this.msgList = res.data.list;
                if (_this.msgList && _this.msgList.length > 0) {
                    let msg = [];
                    _this.msgList.forEach((v, k) => {
                        msg.push(v.title);
                    });
                    _this.msg = msg;
                }
            }).then(v => {
                _this.getbannerList();
            }).catch(v => {
                _this.getbannerList();
            });
        },
        getPopupInfo() {
            let config = uni.getStorageSync('sys_config');
            if (config.popup && config.popup.show_popup) {
                this.popup = config.popup || [];
                this.popup.popup_img =
                    config.popup.popup_img.search('http') >= 0 ? config.popup.popup_img : this.$config.apiImageUrl + config.popup
                    .popup_img;
                this.popup.popupStyle = this.GlobalThemes.backGroundStyle + 'border-radius: 10px; padding: 10px';
                this.$refs.popup.open();
            }
        },
        initPage() {
            if (this.$isAppPlus) {
                this.checkUpdate();
            }
            this.hotVideos = '';
            this.ihotVideos = '';
            this.acgViedeos = '';
            this.varietyViedeos = '';
            let _this = this;
            uni.getSystemInfo({
                success: res => {
                    let query = uni.createSelectorQuery().in(_this);
                    query.select('#uTabbar').boundingClientRect();
                    query.exec(re => {
                        let uTabbarHeight = re[0].height;
                        _this.swiperheight = res.windowHeight - res.statusBarHeight -
                            uTabbarHeight - uni.upx2px(230);
                    });
                }
            });
            if (_this.$isAppPlus) {
                MyApp.showLoading(_this.$refs.loading);
            }
            this.getMsgList();
            uni.stopPullDownRefresh();
        },
        tabChange(e) {
            let index = e.detail.current;
            this.tabIndex = index;
            uni.setStorageSync('tabIndex', index);
        },
        change(index) {
            this.tabIndex = index;
        },
        getVideoList() {
            let _this = this;
            _this.$http.post('/videos/videos/getRecommendList', {}).then(res => {
                if (_this.$isAppPlus) {
                    MyApp.closeLoading(_this.$refs.loading); 
                }
                _this.recommendVideos = res.data; // 电影
            }).then(v => {
                _this.getMoviesList();
            }).then(v => {
                _this.getTvViedeosList();
            }).then(v => {
                _this.getAcgViedeosList();
            }).then(v => {
                _this.getVarietyViedeosList();
            });
        },
        getRecommendMore: util.throttle(function(e) {
            this.recommend.page = this.recommend.page + 1;
            this.getRecommendList();
        }, 800),
        // 电影
        getRecommendList() {
            let _this = this;
            if (!_this.isEnd) {
                _this.loading = true;
                _this.$http.post('/videos/videos/getRecommendList', {
                    page: _this.recommend.page,
                    limit: _this.recommend.limit
                }).then(res => {
                    _this.loading = false;
                    if (res.data.list.length > 0) {
                        res.data.list.forEach((v, k) => {
                            _this.recommendVideos.list.push(v);
                        });
                    } else {
                        _this.recommend.isEnd = true;
                    }
                });
            }
        },
        // 电影
        getMoviesList() {
            let _this = this;
            _this.loading = true;
            _this.$http.post('/videos/videos/getMoviesList').then(res => {
                _this.loading = false;
                _this.hotVideos = res.data; // 电影
            });
        },
        // 电视剧
        getTvViedeosList() {
            let _this = this;
            _this.loading = true;
            _this.$http.post('/videos/videos/getTvViedeosList').then(res => {
                _this.loading = false;
                _this.ihotVideos = res.data; // 电视剧
            });
        },
        // 动漫
        getAcgViedeosList() {
            let _this = this;
            _this.loading = true;
            _this.$http.post('/videos/videos/getAcgViedeosList').then(res => {
                _this.loading = false;
                _this.acgViedeos = res.data; // 动漫
            });
        },
        // 综艺
        getVarietyViedeosList() {
            let _this = this;
            _this.loading = true;
            _this.$http.post('/videos/videos/getVarietyViedeosList').then(res => {
                _this.loading = false;
                _this.varietyViedeos = res.data;
            });
        },
        btnHide () {
            this.dragHide = true;
        },
        btnClick() {
            if (!this.dragHide) {
                this.$refs.drag.navTo();
            }
            this.dragHide = false;
        }
    }
};
