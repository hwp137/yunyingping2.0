import Vue from 'vue'
import App from './App'
import requestApi from '@/common/vmeitime-http/http.js';
import configs from '@/common/config.js';
// 全局分享
import mixin from '@/utils/base.js'
Vue.mixin(mixin)

import uView from "uview-ui";
Vue.use(uView);

Vue.config.productionTip = false
Vue.prototype.$http = requestApi;
Vue.prototype.$config = configs;

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
