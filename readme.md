## 简介

该版本前端使用uniapp开发，后端为php（[one-php](https://gitee.com/chsuperman/one-php)后台系统管理框架，基于tp5.1.*版本），目前已兼容微信小程序、双端APP及H5，功能健全，拆封即用。

该源码分两个版本1.0和2.0，2.0版本为1.0版本的重构版，主要重构了界面UI及系统架构，对于1.0版本来说，重构版除原有的API管理后台外，新增了版本控制系统、升级包制作工具、前端UNIAPP自助升级（易语言），实现用户自助升级，弥补了1.0版本下载安装包手动升级麻烦的问题，避免因为用户错误升级操作导致系统出现bug。

技术栈：vue2 + php + 易语言（可选）

>   兑换码搭建教程
>   https://www.aliyundrive.com/s/shphFPogUt1 提取码：617k
>
>   api搭建教程.mp4（老版本）
>   https://www.aliyundrive.com/s/LBD8XiGrAre 提取码：617k



## 环境搭建

若需要对api站点进行版本控制管理，做矩阵的，需要先搭建“版本控制”后台。

若不需要，直接搭建“api系统”即可。api后台的系统设置、系统更新里的授权状态不用管。

**每个目录下都有readme文件，遇到首页不显示内容的，readme！！！遇到详情页无法播放的，readme！！！各种问题，readme！！！**



# 注意！！！

**若你的api管理系统出现代码被删除的问题，按照下面方法处理：**

**打开`\library\Base.php` ，往下翻找到 `loadModel` 方法，将方法内的代码注释或删除。**

该功能是结合版本控制系统（以下简称`云端`）使用的，逻辑是api接口系统对接`云端`，若关键文件md5值不匹配，则提示非法或删除文件，删除文件开关在`云端`设置。若你只是想自己用， 不想要`云端`系统，那么直接把 `loadModel` 方法内的代码删除掉就可以了，注意：**只删方法内的代码，**`loadModel` **方法不可删除，否则会报错。** 当然你若是有能力修改源码的话，也可以直接删掉这段代码，不过建议不要删除`loadModel`方法，因为在多处文件内都有注入此方法。之所以有这个功能，是因为以前会有坏人搞破坏，不得已而为之，当然现在全开源了，也就无所谓了。

为避免一些人实在找不到，该方法现在已被注释。

`library\Base.php` 里的 `AppResiger` 类主要功能是与`云端`校验文件md5和执行相应的规则。





## 更新日志

后续暂无更新计划。



### 2022-8-28更新【暂不考虑开放！】

修复H5、APP端种种问题。

安卓端更换ijkplayer播放器，更新uni官方APP在线热更新及版本控制后台。

https://ver.617kan.cn



### 2022-8-18更新

小程序登录后不显示头像昵称问题

>   pages/user/index.vue：
>
>    `<open-data type="chooseAvatar"></open-data>`
>
>   改
>
>   `<image :src="userInfo.avatar ? userInfo.avatar : '../../static/icons/avatar.png'"></image>`
>    
>    
>    
>    `<open-data type="userNickName"></open-data>`
>    
>    改
>
>   `<text>{{ userInfo.username }}</text>`



### 2022-7-13更新

修复小程序广告不展示问题

>    已安装的需手动修复，在数据库内执行以下代码即可
>
>    ALTER TABLE `one_ad` 
>    	ADD COLUMN `group_id` int(10) NOT NULL DEFAULT 0 COMMENT '会员分组' AFTER `url`;





### 如果该项目对你有帮助，请点个star吧！♥



##### 其他

[商业版微信小程序商城](http://saasshopdemo.zskey.com/)

[云影评切片系统](https://gitee.com/lyq617/yunyingping_qiepian_beta)
