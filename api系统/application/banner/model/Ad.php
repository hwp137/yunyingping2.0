<?php

namespace app\banner\model;

use app\common\model\Base;

class Ad extends Base
{
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg' => '非法操作！', 'code' => 711]));
        }
    }
    /**
     * 随机取一条
     * type 3 弹窗广告
     * 4 常驻广告
     * @param array $map
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getOne($type = 0, $min = 1, $userinfo)
    {
        $num_key = md5($type . $min . $userinfo['group_id'] . $userinfo['id']);
        $num = cache($num_key) != null ? cache($num_key) : 0;
        // var_dump($num);
        $map = [];
        $map[] = ['status', 'eq', 1];
        $map[] = ['type', 'eq', $type];
        $map[] = ['group_id', 'eq', $userinfo['group_id']];

        if ($min == 1) {
            if ($num >= config('playad.play_ad_num')) {
                $map = [];
                $map[] = ['status', 'eq', 1];
                $map[] = ['type', 'eq', $type];
                $map[] = ['group_id', 'eq', 0];
                $obj = $this->where($map)->orderRaw('rand()');
                $count = $obj->count();
                $data = $count > 0 ? $obj->limit(1)->select() : [];
                cache($num_key, $num + 1, 86400);
                return $data;
            }
            $obj = $this->where($map)->orderRaw('rand()');
            $count = $obj->count();
            $data = $count > 0 ? $obj->limit(1)->select() : [];
            if (empty($data)) {
                $map = [];
                $map[] = ['status', 'eq', 1];
                $map[] = ['type', 'eq', $type];
                $map[] = ['group_id', 'eq', 0];
                $obj = $this->where($map)->orderRaw('rand()');
                $count = $obj->count();
                $data = $count > 0 ? $obj->limit(1)->select() : [];
            }
            cache($num_key, $num + 1, 86400);
            return $data;
        } else {
            if ($num >= config('playad.play_ad_num')) {

                $map = [];
                $map[] = ['status', 'eq', 1];
                $map[] = ['type', 'eq', $type];
                $map[] = ['group_id', 'eq', 0];
                $obj = $this->where($map)->orderRaw('rand()');
                $count = $obj->count();
                $data = $count > 0 ? $obj->limit(0, $min)->select() : [];
                cache($num_key, $num + 1, 86400);
                return $data;
            }
            $obj = $this->where($map)->orderRaw('rand()');
            $count = $obj->count();
            $data = $count > 0 ? $obj->limit(0, $min)->select() : [];
            if (empty($data)) {
                $map = [];
                $map[] = ['status', 'eq', 1];
                $map[] = ['type', 'eq', $type];
                $map[] = ['group_id', 'eq', 0];
                $obj = $this->where($map)->orderRaw('rand()');
                $count = $obj->count();
                $data = $count > 0 ? $obj->limit(0, $min)->select() : [];
            }
            cache($num_key, $num + 1, 86400);
            return $data;
        }
        // $count = $this->where($map)->count();
        // if ($count > $min && $min == 1) {
        //     // 随机取一条
        //     $sql = 'SELECT * FROM `'.config('database.prefix').'ad` 
        //         AS t1 JOIN (SELECT ROUND(RAND() * ((SELECT MAX(id) FROM `'.config('database.prefix').'ad`)-(SELECT MIN(id) FROM `'.config('database.prefix').'ad`))+(SELECT MIN(id) FROM `'.config('database.prefix').'ad`)) AS id) AS t2
        //         WHERE t1.id >= t2.id
        //         AND status = 1 AND type = '.$type.'
        //         ORDER BY t1.id LIMIT 1';
        //     $r = $this->query($sql);
        //     return $r ? $r[0] : [];
        // }else{
        //     if ($count == 0) {
        //         return [];
        //     }
        //     if ($min > 1) {
        //         // 随机取一条
        //         $sql = 'SELECT * FROM `'.config('database.prefix').'ad` 
        //             AS t1 JOIN (SELECT ROUND(RAND() * ((SELECT MAX(id) FROM `'.config('database.prefix').'ad`)-(SELECT MIN(id) FROM `'.config('database.prefix').'ad`))+(SELECT MIN(id) FROM `'.config('database.prefix').'ad`)) AS id) AS t2
        //             WHERE t1.id >= t2.id
        //             AND status = 1 AND type = '.$type.'
        //             ORDER BY t1.id LIMIT ' . $min;
        //         $r = $this->query($sql);
        //         return $r ? $r : [];
        //         // return $this->where($map)->limit($min)->select();
        //     }
        //     return $this->where($map)->find();
        // }
    }

    public function getImgAttr($v, $d)
    {
        return explode(',', $v);
    }
}
