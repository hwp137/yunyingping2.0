<?php

namespace app\tabbar\admin;

use app\system\admin\Admin;
use app\tabbar\model\Tabbar as TabbarModel;
use app\tabbar\model\TabbarType as TabbarTypeModel;

class Index extends Admin
{

    protected $oneModel = 'Tabbar';

    public function index()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;
            $data = (new TabbarModel)->getList($map, $page, $limit);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        $this->assign('type', TabbarTypeModel::getFormatList());
        return parent::edit($id);
    }

    public function def($id = 0, $val)
    {
        if ($val == 1) {
            (new TabbarModel)->where('1=1')->setField('is_default', 0);
        }
        (new TabbarModel)->where('id', $id)->setField('is_default', $val);
        if (!(new TabbarModel)->where('is_default', 1)->find()) {
            (new TabbarModel)->where('id', $id)->setField('is_default', 1);
            return $this->error('必须留一个默认首页');
        }
        return $this->success('修改成功');
    }
}
