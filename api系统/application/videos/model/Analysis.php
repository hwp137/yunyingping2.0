<?php
namespace app\videos\model;

use app\common\model\Base;

class Analysis extends Base
{

    protected $name = "video_analysis";

    protected $auto = ['unique'];

    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }

}