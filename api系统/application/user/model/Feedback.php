<?php
namespace app\user\model;

use app\common\model\Base;

/**
 * 会员模型
 * @package app\user\model
 */
class Feedback extends Base
{
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }

    public function getImgsAttr($v, $d)
    {
        return $v ? explode(',', $v) : [];
    }
}