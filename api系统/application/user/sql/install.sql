/*
 sql安装文件
*/
CREATE TABLE `one_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '父级id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员分组ID',
  `nick` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` bigint(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '手机号',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码混淆',
  `money` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '可用金额',
  `frozen_money` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '冻结金额',
  `income` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '收入统计',
  `expend` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '开支统计',
  `exper` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经验值',
  `integral` int(10) UNSIGNED NOT NULL DEFAULT 100 COMMENT '积分',
  `frozen_integral` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '冻结积分',
  `sex` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '性别(1男，0女)',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `wxmp_openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mp_openid',
  `has_bind` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否已绑定用户名 0 待绑定 1 已绑定',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注信息',
  `last_login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登陆IP',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登陆时间',
  `login_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登陆次数',
  `expire_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '到期时间(0永久)',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0禁用，1正常)',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100001 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '[系统] 会员表' ROW_FORMAT = Compact;

CREATE TABLE `one_user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL COMMENT '等级名称',
  `min_exper` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最小经验值',
  `max_exper` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大经验值',
  `discount` int(2) unsigned NOT NULL DEFAULT '100' COMMENT '折扣率(%)',
  `intro` varchar(255) NOT NULL COMMENT '等级简介',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '默认等级',
  `expire` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员有效期(天)',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0',
  `mtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='[系统] 会员等级';

INSERT INTO `one_user_group` (`name`, `min_exper`, `max_exper`, `discount`, `intro`, `default`, `expire`, `status`, `ctime`, `mtime`)
VALUES
  ('注册会员', 0, 0, 100, '', 1, 0, 1, 0, 1545105600);

CREATE TABLE `one_user_token` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`uid` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户id',
	`token` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '用户token',
	`create_time` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '修改时间',
	`expire_time` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '到期时间',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户token';

CREATE TABLE `one_user_wallet` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`uid` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户id',
	`money` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '返利',
	PRIMARY KEY (`id`)
) COMMENT='推广获利（用户钱包）' COLLATE='utf8_general_ci' ENGINE=MyISAM;

CREATE TABLE `one_user_wallet_log` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
	`uid` MEDIUMINT(8) UNSIGNED NOT NULL,
	`value` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT '变动金额',
	`msg` TEXT NOT NULL,
	`dateline` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`type` VARCHAR(20) NOT NULL DEFAULT '',
	`admin_id` INT(10) UNSIGNED NULL DEFAULT '0',
	`money_detail` TINYTEXT NOT NULL COMMENT '余额明细',
	PRIMARY KEY (`id`) USING BTREE
) COMMENT='财务变动记录表' COLLATE='utf8_general_ci' ENGINE=MyISAM;

CREATE TABLE `one_feedback`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '留言类型',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '留言内容',
  `imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '留言图片',
  `contact` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系方式',
  `replay` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '处理反馈',
  `is_read` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否已读 0 未读 1 已读',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户反馈' ROW_FORMAT = Dynamic;

CREATE TABLE `one_user_like`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '会员id',
  `vid` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '视频id',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact COMMENT = '用户收藏';

CREATE TABLE `one_user_behavior` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '类型 1 截图',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '其他信息',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态 0 未处理 1 已处理',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户行为记录';