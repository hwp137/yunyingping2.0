<?php
namespace app\user\server;
use app\common\server\Service;
use app\user\validate\User as UserValidate;
use app\user\model\Feedback as FeedbackModel;
use app\user\model\User as UserModel;

class User extends Service{

    public function initialize() {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>712]));
        }
        $this->UserValidate = new UserValidate();
        $this->FeedbackModel = new FeedbackModel();
        $this->UserModel = new UserModel();
    }

    public function feedback($data, $user) {
        if($this->UserValidate->scene('feedback')->check($data) !== true) {
            $this->error = $this->UserValidate->getError();
            return false;
        }
        if ($user) {
            $data['contact'] = $user['id'];
        }
        if ($this->FeedbackModel->isUpdate(false)->save($data)) {
            return true;
        }
        return false;
    }

    public function getFeedbackList($data, $user)
    {
        $map = [];
        $map[] = ['contact', 'eq', $user['id']];
        $page = isset($data['page']) ? $data['page'] : 1;
        $limit = isset($data['limit']) ? $data['limit'] : 20;
        $map['cache'] = false;
        return $this->FeedbackModel->getList($map, $page, $limit, 'is_read asc, create_time desc');
    }

    public function getFeedbackDetail($data, $user)
    {
        $map = [];
        $map[] = ['id', 'eq', $data['id']];
        $info = $this->FeedbackModel->where($map)->find();
        if ($info) {
            if (!empty($info->replay)) {
                $info->is_read = 1;
                $info->save();
            }
            return $info;
        }
        $this->error = "获取失败";
        return false;
    }

    public function changeMobile($data, $user) {
        // 校验验证码
        # todo
        if ($this->UserModel->isUpdate(true)->save($data, ['id'=>$user['id']])) {
            return true;
        }
        return false;
    }

    public function changePassword($data, $user) {
        if ($data['password'] != $data['repassword']) {
            $this->error = "两次密码不一致";
            return false;
        }
        // 校验验证码
        # todo
        $data['salt'] = $this->UserModel->where('id', $user['id'])->value('salt');
        $old_password = $this->UserModel->where('id', $user['id'])->value('password');
        if ($old_password != md5(md5($data['oldpassword']).$data['salt'])) {
            $this->error = "旧密码不正确";
            return false;
        }
        if ($old_password == md5(md5($data['password']).$data['salt'])) {
            $this->error = "新密码不可与旧密码一样";
            return false;
        }
        if ($this->UserModel->isUpdate(true)->allowField(true)->save($data, ['id'=>$user['id']])) {
            return true;
        }
        return false;
    }
    
    public function changeUsi($data, $user)
    {
        if (empty($data['nick']) && empty($data['avatar'])) {
            return true;
        }
        if ($this->UserModel->isUpdate(true)->save($data, ['id'=>$user['id']])) {
            return true;
        }
        return false;
    }
    
    public function getNumbers($data, $user)
    {
        return $this->UserModel->where('id', $user['id'])->value('number');
    }
    
    public function home($data, $user){
        return $user ? $user : 0;
    }

    public function bindUser($data, $user)
    {
        if ($this->UserModel->where('username', $data['username'])->value('id')) {
            $this->error = "该用户名已被注册~";
            return false;
        }
        $info = $this->UserModel->where('id', $user['id'])->find();
        if (!$info) {
            $this->error = "未找到用户或未登录";
            return false;
        }
        
        $info->username = $data['username'];
        $info->has_bind = 1;
        return $info->save();
    }
}