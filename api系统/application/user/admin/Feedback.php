<?php
namespace app\user\admin;

use app\system\admin\Admin;
use app\user\model\Feedback as FeedbackModel;
/**
 * 会员控制器
 * @package app\user\admin
 */
class Feedback extends Admin
{
    protected $oneModel = 'Feedback';
    /**
     * 会员管理
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $where      = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');
            if ($keyword) {
                $where[] = ['contact', 'eq', $keyword];
            }
            $where['cache'] = false;
            $data = (new FeedbackModel)->getList($where, $page, $limit);
            foreach ($data['list'] as $key => $value) {
                $data['list'][$key]['imgs'] = $value['imgs'];
            }
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }

    public function replay($id = 0, $val = '')
    {
        $info = (new FeedbackModel)->where('id', $id)->find();
        if ($info) {
            $info->replay = $val;
            $info->save();
            return $this->success('已回复');
        }
        return $this->error('回复失败');
    }

}