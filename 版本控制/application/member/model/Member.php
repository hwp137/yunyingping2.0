<?php

namespace app\member\model;

use think\model\concern\SoftDelete;

class Member extends Base
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $type = [
        'login_time'    =>  'timestamp',
        'last_login_time'     =>  'timestamp',
        'ext_conifg'     =>  'json',
    ];

    /**
     * 重置密码
     * @param string $password
     * @param $condition
     * @return array
     */
    public function resetMemberPassword($password = '123456', $condition)
    {
        $salt = random(6, 0);
        $res = $this->update([
            'salt' => $salt,
            'password' => md5($salt . $password)
        ], $condition);
        runhook('repwd_after');
        return $res;
    }
}
