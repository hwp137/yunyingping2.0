<?php

namespace app\upgrade\model;

class UpgradeFile extends Base
{
    /**
     * 下载指定文件
     * @param  number  $root 文件存储根目录
     * @param  integer $id   文件ID
     * @param  string   $args	 回调函数参数
     * @return boolean	   false-下载失败，否则输出下载文件
     */
    public function download($id, $callback = null, $args = null)
    {
        /* 获取下载文件信息 */
        $file = $this->find($id);
        if (!$file) {
            $this->error = '不存在该文件！';
            return false;
        }
        /* 下载文件 */
        switch ($file['location']) {
            case 0: //下载本地文件
                return $this->downLocalFile($file, $callback, $args);
            case 1: //下载FTP文件
                return $this->downFtpFile($file, $callback, $args);
                break;
            default:
                $this->error = '不支持的文件存储类型！';
                return false;
        }
    }

    /**
     * 下载本地文件
     * @param  array	$file	 文件信息数组
     * @param  callable $callback 下载回调函数，一般用于增加下载次数
     * @param  string   $args	 回调函数参数
     * @return boolean			下载失败返回false
     */
    public function downLocalFile($file, $callback = null, $args = null)
    {
        $filename = DOC_ROOT . $file['savepath'] . $file['savename'];
        if (is_file($filename)) {
            /* 调用回调函数新增下载数 */
            is_callable($callback) && call_user_func($callback, $args);
            // /* 执行下载 */ //TODO: 大文件断点续传
            header("Content-Description: File Transfer");
            header('Content-type: ' . $file['type']);
            header('Content-md5:' . $file['md5']);
            header('Content-Length:' . $file['size']);
            // 这个注释打开就无法下载
            // if (preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT']) || strpos($_SERVER['HTTP_USER_AGENT'], "Triden")) { //for IE
            //     header('Content-Disposition: attachment; filename="' . rawurlencode($file['name']) . '"');
            // } else {
            //     header('Content-Disposition: attachment; filename="' . $file['name'] . '"');
            // }
            header('Content-Disposition: attachment; filename="' . $file['name'] . '"');
            @readfile($filename);
            exit;
        } else {
            $this->error = '文件已被删除！';
            return false;
        }
    }

    /**
     * 下载ftp文件
     * @param  array	$file	 文件信息数组
     * @param  callable $callback 下载回调函数，一般用于增加下载次数
     * @param  string   $args	 回调函数参数
     * @return boolean			下载失败返回false
     */
    private function downFtpFile($file, $callback = null, $args = null)
    {
        /* 调用回调函数新增下载数 */
        is_callable($callback) && call_user_func($callback, $args);

        $host = cache('DOWNLOAD_HOST.host');
        $root = explode('/', $file['rootpath']);
        $file['savepath'] = $root[3] . '/' . $file['savepath'];

        $data = array($file['savepath'], $file['savename'], $file['name'], $file['mime']);
        $data = json_encode($data);
        $key = think_encrypt($data, cache('DATA_AUTH_KEY'), 600);

        header("Location:http://{$host}/onethink.php?key={$key}");
    }
    /**
     * 上传文件
     *
     * @param [type] $info
     * @param [type] $base_file
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function uploadFile($info, $base_file = './upload')
    {
        $md5 = $info->md5();
        $file_info = $info->getInfo();
        $file_name = $info->getFilename();
        $savepath = str_replace('.', '', $base_file);
        $data = [
            'md5'       => $md5,
            'savename'  => $file_name,
            'savepath'  => $savepath,
            'name'      => $file_info['name'],
            'type'      => $file_info['type'],
            'size'      => $file_info['size'],
            'create_time' => time()
        ];
        if ($file_id = $this->getFieldByMd5($md5, 'id')) {
            return (int)$file_id;
        }
        if (($file_id = $this->insertGetId($data))) {
            return (int)$file_id;
        }
    }

    public function getLocationTextAttr($v, $d)
    {
        $arr = [0 => '本地', 1 => '云端'];
        return $arr[$d['location']];
    }
}
