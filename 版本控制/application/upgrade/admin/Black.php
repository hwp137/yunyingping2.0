<?php

namespace app\upgrade\admin;

use app\system\admin\Admin;
use app\upgrade\model\UpgradeMd5Log;

class Black extends Admin
{

    protected $oneModel = 'UpgradeMd5Log';

    protected function initialize()
    {
        parent::initialize();
        $this->UpgradeMd5Log = new UpgradeMd5Log;
    }

    public function index()
    {
        if ($this->request->isAjax()) {
            $condition = [];
            $page = input('page/d', 1);
            $limit = input('limit/d', 15);
            $order = 'create_time DESC';
            $result = $this->UpgradeMd5Log->pageList($condition, true, $order, $page, $limit);
            return $this->success('获取成功', '', $result);
        }
        return $this->fetch();
    }
}
